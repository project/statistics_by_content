<?php

namespace Drupal\statistics_by_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config form file.
 */
class ConfigForm extends ConfigFormBase {
  /**
   * Config settings.
   */
  const SETTINGS = 'statistics_by_content.config';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'statistics_by_content_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS);

    $entityTypeManager = \Drupal::service('entity_type.manager');

    $types = [];
    $contentTypes = $entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $types[$contentType->id()] = $contentType->label();
    }

    $form['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $types,
      '#default_value' => $config->get('content_type'),
      '#multiple' => TRUE,
      '#description' => $this->t('Select content types to apply statistics'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable(static::SETTINGS)
      ->set('content_type', $form_state->getValue('content_type'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
